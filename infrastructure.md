# infrastructure

## website

- landing page at https://acdw.casa/beans.html to be moved to the main domain,
  https://43beans.casa/

- acdw and wsinatra have offered to host infra if needed

- CI/CD, Packaging, etc. Pretty much any automation wsinatra is happy to help
  with

## git repo, CI/CD

- git repos currently at: https://notabug.org/43beans

- it was initially proposed to host a wiki and git repo at tildegit.org.
  unfortunately, it was later discovered that the instance requires an email
  from one of the tildeverse-recognised tildes to sign up, which presents an
  access hurdle for people who do not have accounts from other tildes.

- other suggestions: notabug.org, codeberg.org. sourcehut (sr.ht) is currently
  free to make an account but may be subscription-based later, though it is
  self-hostable. fossil-scm.org was also mentioned as another long-term
  self-hostable option.

- wsinatra has offered to make a lapis app to run ci builds to complement the
  git hosting, and tkts is available for issue tracking if needed.

- another idea: do we want to try and setup a shared dev setup using Nix?
  (wsinatra is happy to do this if it helps anyone)
