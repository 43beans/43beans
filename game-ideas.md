# game ideas

2022-05-09: the following are ideas collected during the brainstorming phase
for the first game.

## hako

- make a [hako] game! player vs computer, 2p co-op, 2p network.

[hako]: https://wiki.xxiivv.com/site/hako.html

## Fibber

front-end for fibs

## Back to the Gammon II: Backier Gammon

- Time traveling backgammon RPG adventure
- see also [tildetown zine #5] (p.31) for gameplay details

[tildetown zine #5]: http://tilde.town/~zine/issues/5/zine.pdf

## Corpse Boy Loves Ghost Girl

one-player or two-player game where you can switch back and forth between CB
and GG like in Donkey Kong Country or *Aaahh! Real Monsters*. Corpse Boy is
tangible and can interact with objects, and Ghost Girl can pass through
walls/doors and briefly possess NPCs/guards to get them to open doors and
stuff.

## Rollersnake

- side scroller, collect coins, avoid obstacles
- baddies are cowboys: "snakes should be in boots not skates"

## Giraffe Wants Best Leaves

- you are a giraffe who wants the best leaves but your neck is too short!
- a port of the [original ttrpg] with more graphics and backstory

[original ttrpg]: https://dozens.itch.io/there-is-a-giraffe-who-wants-the-best-leaves-but-its-neck-is-too-short

## untitled story game rpg

- the legend of bean

## Baba Yaga Goes To Space

- Front end for [Eli's game]

[Eli's game]: https://git.sr.ht/~eli_oat/babayagagoestospace

## Damage Tamale

[pronounciation guide](http://ipa-reader.xyz/?text=d%C9%99-m%C3%A4%E2%80%B2%CA%92e%20t%C9%99-m%C3%A4%E2%80%B2le)

- idk, i just tried to type 'tamale' and my phone autocorrected it to 'damage'.
  there's a game in there somewhere right?
- not op but here are totally not-serious random ideas *(ed. note: mio, in
  response to dozens?)*

- side-scrolling platformer with multiple levels
  - goal: collect tamales from a pot before it explodes
  - setting: a cooking school. for some reason every class is making tamales
    for a cook-off/festival. the school has been taken over by animate kitchen
      equipment and it is up to the character to save the tamales
  - gameplay: each level has a timer. character navigates through obstacles
    made of precariously-placed tableware and sauce spills, while avoiding
    common enemies of all good chefs like walking timers with ear-splitting
    sound attack and knife-throwing knife blocks. collect tamales near the end
    of the level and race to the exit to clear the level. collect corn cobs to
    restore health mid-level

- block-matching/clearing puzzle
  - goal: collect tamales from a pot before it explodes
  - setting: a kitchen?
  - gameplay: each level has a temperature meter. the board contains corn
    kernels of different colours/shapes. player matches corn in specific
    formations to grind enough masa for the next batch of tamales in order to
    clear a level. there is a pot of tamales boiling while the player collects
    corn for masa. making bad matches, e.g. having run out of moves making
    desired matches, will cause the grinding to take longer and the water
    temperature in the pot to go up. if the water gets too hot, the pot will
    explode (game over). scoring is based on the amount of tamales collected
    and multipliers from multiple matches of the same kind in succession.
  - extended scope: tamales can be eaten or traded with npc neighbour for items
    that have effect on gameplay (powerups). eating tamales increases a
    happiness meter, which when filled gives the player fortitude to redo the
    level (another chance).
  - probably too big in scope for a first game. why would different ratios of
    corn kernels take longer to grind? idk, the machine is an ancient
    contraption that barely works while grumbling and not very good?

- inventory management x trading card game
  - goal: become the best tamale sommelier ever
  - setting: shop specialising in tamales, various locations
  - gameplay: find and collect different tamale recipes, take on the challenges
    of local/regional/international epicureans and serve the most delicious
    tamale feasts. complete challenges by selecting and assembling tamale
    courses based on the epicurean's specialties and "weaknesses". deal more
    "damage"  with the strategic ordering and grouping of multiple tamales
    (though a little luck also helps).

## Oyster Egg

- hidden easter egg mini-game: find 43 beans *(ed. note: idea from wsinatra)*
- oyster eggs = pearls. which hatch into baby oysters.
- a jewel thief who steals pearls, who is also an oyster trying to rescue all
  the unhatched oysters, but they feel guilty about stealing, so they leave
  quail duck behind.

## tactical RPG

- like Final Fantasy Tactics *(ed. note: idea from wsinatra)*

## Carpe Beanum

idk, i said it in irc and people seemed to like it
