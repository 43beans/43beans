## about

[website] ~ [source] ~ [publishing]

we are a [tilde]-adjacent group of friends making games for fun.

irc: `#43beans at `[m455.casa]

[website]: https://43beans.casa/
[source]: https://codeberg.org/43beans
[publishing]: https://43beans.itch.io/
[tilde]: http://tilde.town/~dozens/podcast/about.html
[m455.casa]: https://m455.casa/irc/

## tell me more!

- [first game: hako](hako)
- [infra planning](infrastructure.md)
- [game ideas](game-ideas)
