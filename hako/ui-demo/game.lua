------------------------------------------------------------------------------
-- Game module
------------------------------------------------------------------------------

local util = require("util")

local game = {}


-- Initialize a grid for die positioning.
function game.setup_grid(cols, rows)
  local grid = {}
  for c = 1, #cols do
    for r = 1, #rows do
      -- { 0, 0, 0, 0 } represents an empty position on the grid
      grid[c][r] = { 0, 0, 0, 0 }
    end
  end
  return grid
end


-- Select a starting player.
function game.pick_start_player(players)
  math.randomseed(os.time())
  return players[util.table_keys(players)[math.random(1, #players)]]
end


-- Return a table of 4 numbers representing a die face with 4 triangular edges
-- in 4 directions. Output is in the form { north, east, south, west }, where
-- each number is either 1 (yellow) or 2 (blue).
function game.roll_die()
  math.randomseed(os.time())
  return { math.random(1, 2), math.random(1, 2), math.random(1, 2),
    math.random(1, 2) }
end


-- Return the adjacent positions of a given grid position.
function game.get_adj_pos(pos)
  return { { pos[1], pos[2] - 1 }, { pos[1] + 1, pos[2] },
    { pos[1], pos[2] + 1 }, { pos[1] - 1, pos[2] } }
end


-- Check whether a position has adjacent dice edges. Return true and the
-- positions if there are adjacent edges, false otherwise.
function game.has_adj_edges(grid, pos)
  local adj_edges = {}
  local adj_pos = game.get_adj_pos(pos)
  for p = 1, #adj_pos do
    if grid[p1][p2] ~= 0 then
      table.insert(adj_pos, p)
    end
  end
  if #adj_pos ~= 0 then
    return true, adj_pos
  else
    return false
  end
end


-- Check if a move meets certain conditions to be considered valid. Return true
-- if valid, or false otherwise.
-- player_hist[m] = { { 0, 0, { 0, 0, 0, 0 } }, { 0, 0, { 0, 0, 0, 0 } } }
function game.is_valid_move(grid, player_hist, move)
  local srt_pos = move[1]
  local end_pos = move[2]
  -- Out of grid range
  if ((end_pos[1] < 1) and (end_pos[1] > #grid)) or ((end_pos[2] < 1) and
    (end_pos[2] > #grid[1])) then
    do return false end
  -- Position is non-empty
  elseif grid[end_pos[1]][end_pos[2]] ~= { 0, 0, 0, 0 } then
    do return false end
  -- Not adjacent to any other edges
  elseif not game.has_adj_edges(grid, end_pos) then
    do return false end
  -- Same die being moved consecutively
  elseif (srt_pos[1] == player_hist[#player_hist][2][1]) and
    (srt_pos[2] == player_hist[#player_hist][2][2]) then
    do return false end
  end
  -- Check the adjacent dice around the starting position. If any of the
  -- adjacent dice has only one adjacent edge, then moving the die would leave
  -- it hanging and result in an invalid move.
  local prev_adj_pos = game.get_adj_pos(srt_pos)
  for p = 1, #prev_adj_pos do
    local _, adj_edges = game.has_adj_edges(grid, p)
    if (grid[p[1]][p[2]] ~= 0) and (#adj_edges == 1) then
      do return false end
    end
  end
  return true
end


-- Return the latest positions of all dice on the grid from a dice index.
function game.get_dice_pos(dice)
  local dice_pos = {}
  for d = 1, #dice do
    table.insert(dice_pos, dice[d][#dice[d]])
  end
  return dice_pos
end


-- Return a table of valid positions.
function game.get_valid_pos(grid, dice, player_hist, move)
  local dice_pos = game.get_dice_pos(dice)
  local adj_pos = {}
  local valid_pos = {}
  for p = 1, #dice_pos do
    adj_pos = game.get_adj_pos(p)
    for a = 1, #adj_pos do
      if game.is_valid_move(grid, player_hist, { move[1], a }) then
        table.insert(valid_pos, a)
      end
    end
  end
  return valid_pos
end


-- Rotate die face clockwise.
function game.rotate_face(face)
  -- Single-color faces have no rotation changes
  if (face == { 1, 1, 1, 1 }) or (face == { 2, 2, 2, 2 }) then
    do return face end
  -- Alternating colors
  elseif face == { 1, 2, 1, 2 } then do return { 2, 1, 2, 1 } end
  elseif face == { 2, 1, 2, 1 } then do return { 1, 2, 1, 2 } end
  end
  -- Other faces
  local rot_face = {}
  rot_face[1] = face[4]
  for d = 2, (#face - 1) do
    table.insert(rot_face, #face[d])
  end
  return rot_face
end


-- Position a die in the grid.
function game.place_die(grid, dice, player_hist, move, move_count)
  local g = grid
  local d = dice
  local h = player_hist
  -- Add to grid
  g[move[2][1]][move[2][2]] = move[2][3]
  g[move[1][1]][move[1][2]] = { 0, 0, 0, 0 }
  -- Update dice position index
  -- Round 1
  if move_count <= 8 then
    d[move_count] = {}
    table.insert(d[move_count], { move[2][1], move[2][2], move[2][3] })
  -- Round 2
  else
    for d = 1, #dice do
      if (dice[d][#dice[d]][1] == move[1][1]) and
        (dice[d][#dice[d]][2] == move[1][2]) then
        table.insert(dice[d], { move[2][1], move[2][2], move[2][3] })
        break
      end
    end
  end
  -- Update history
  table.insert(h, move)
  return g, d, h
end


-- Get the colors of the edges around a die, starting with the N die's S edge
-- and clockwise to the W die's E edge.
function game.get_adj_colors(grid, pos)
  local adj_pos = game.get_adj_pos(pos)
  local adj_colors = {
    grid[adj_pos[1][1]][adj_pos[1][2]][3],
    grid[adj_pos[2][1]][adj_pos[2][2]][4],
    grid[adj_pos[3][1]][adj_pos[3][2]][1],
    grid[adj_pos[4][1]][adj_pos[4][2]][2],
  }
  local y_count = 0
  local b_count = 0
  for c = 1, #adj_colors do
    if adj_colors[c] == 1 then
      y_count = y_count + 1
    elseif adj_colors[c] == 2 then
      b_count = y_count + 1
    end
  end
  return y_count, b_count, adj_colors
end


-- Given the player of the most recent turn, check the grid for a win or draw
-- and return the result.
function game.check_result(grid, dice, turn)
  local dice_pos = game.get_dice_pos(dice)
  local y_count = 0
  local b_count = 0
  local all_y = false
  local all_b = false
  for d = 1, #dice_pos do
    y_count, b_count, _ = game.get_adj_colors(grid, dice_pos[d])
    if y_count == 4 then all_y = true end
    if b_count == 4 then all_b = true end
  end
  if (all_y) and (not all_b) then
    return { "win", "yellow" }
  elseif (all_b) and (not all_y) then
    return { "win", "blue" }
  elseif (all_y and all_b) then
    return { "draw", turn }
  else
    return {}
  end
end


return game
