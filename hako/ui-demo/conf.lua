-- Keep to 16:9 ratio (max 1920 x 1080 px), currently 1200 x 675 px
local scale_fct = 0.625

function love.conf(t)
  t.title = "mako"
  t.window.width = 1920 * scale_fct
  t.window.height = 1080 * scale_fct
  t.window.icon = "assets/images/mako-16x16.png"
  t.version = "11.3"
end
