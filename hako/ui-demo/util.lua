------------------------------------------------------------------------------
-- Module of utility functions
------------------------------------------------------------------------------

local util = {}


-- Format debug output
function util.debug(tag, msg, debug)
  if debug then
    print("[" .. tag .. "] " .. msg)
  end
end


-- Get the keys in an associative table
function util.table_keys(tbl, sort)
  local keys = {}
  local n = 0
  for k, v in pairs(tbl) do
    n = n + 1
    keys[n] = k
  end
  if (sort == nil) or (sort == true) then table.sort(keys) end
  return keys
end


-- Count the number of times a value appears in a table
function util.count_table_values(tbl, val)
  local count = 0
  for i = 1, #tbl do
    if tbl[i] == val then count = count + 1 end
  end
  return count
end


return util
