# UI demo

Screen test in LÖVE/Lua.

## Instructions

- Install [LÖVE] from the official website or a package manager.
- Change into this directory and run: `love .`

[LÖVE]: https://love2d.org/

## License

[Fair License] or [GCL]

[Fair License]: https://en.wikipedia.org/wiki/Fair_License
[GCL]: https://acdw.casa/gcl/

## Credits

- Poems by [acdw]
- Music and sound effects by [agafnd]
- [Calcutta] font by the [Indian Type Foundry] and licensed under the [OFL (SIL
Open Font License)].

[acdw]: https://acdw.net/
[agafnd]: https://tilde.town/~agafnd/
[Calcutta]: https://fontlibrary.org/en/font/calcutta
[Indian Type Foundry]: http://www.indiantypefoundry.com/
[OFL (SIL Open Font License)]: http://scripts.sil.org/OFL
