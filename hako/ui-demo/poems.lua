local poems = {}

poems.yellow = {
[[Morning has broken open
like an egg.  The yolk splits,
runs off the table, and drips
onto the floor, waiting to be
lapped up by the dog
(She loves mornings).]],
}

poems.blue = {
[[The night tucks us in
and the stars sing a lullaby.
Rocked by the movement of
the spheres, the world fidgets,
fusses for a moment---but soon
becomes still.  The moon dreams.]],
}

return poems
