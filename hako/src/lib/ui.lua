------------------------------------------------------------------------------
-- UI module
------------------------------------------------------------------------------

local ui = {
  fonts_dir = "assets/fonts",
  images_dir = "assets/images",
  fonts_ext = ".otf",
  images_ext = ".png",
  max_width = 1920,
  max_height = 1080,
  grid_unit = 120,
  window_width = 1200,
  window_height = 675,
  assets = {
    images = {},
    fonts = {},
    sounds = {},
  },
}
ui.scale = ui.window_width / ui.max_width
ui.unit = ui.grid_unit * ui.scale


------------------------------------------------------------------------------
-- Asset preloading and scaling
------------------------------------------------------------------------------

-- Initialise assets from a directory of assets files
function ui.init_assets(dir, asset_type)
  -- Get a list of files and load them into the assets table
  -- [TODO] The use of `ls` is probably not cross-platform compatible.
  local files_str = io.popen("ls " .. dir)
  if asset_type == "images" then
    for f in string.gmatch(files_str:read("*a"), "%S+") do
      if string.find(f, ui.images_ext) ~= nil then
        ui.assets.images[string.gsub(f, ui.images_ext, "")] =
          love.graphics.newImage(dir .. "/" .. f)
      end
    end

  elseif asset_type == "fonts" then
    for f in string.gmatch(files_str:read("*a"), "%S+") do
      if string.find(f, ui.fonts_ext) ~= nil then
        ui.assets.fonts[string.gsub(f, ui.fonts_ext, "")] =
          love.graphics.newFont(dir .. "/" .. f)
      end
    end

  end
end


-- Return the coordinates to center an image
function ui.center_pos(img)
  local pos = {}
  pos[1] = math.floor(((ui.max_width / 2) - (ui.assets.images[img]:getWidth()
    / 2)) * ui.scale)
  pos[2] = math.floor(((ui.max_height / 2) - (ui.assets.images[img]:getHeight()
    / 2)) * ui.scale)
  return pos
end


-- Convert the original size of an image to the scaled size
function ui.get_size(img)
  return { math.floor(ui.assets.images[img]:getWidth() * ui.scale),
    math.floor(ui.assets.images[img]:getHeight() * ui.scale) }
end


-- Wrapper around love.graphics.draw with scale factor included
function ui.draw(img, x, y, rot)
  if rot == nil then rot = 0 end
  love.graphics.draw(ui.assets.images[img], math.floor(x), math.floor(y), rot,
    ui.scale, ui.scale)
end


-- Wrapper around love.graphics.font with the scale factor included
function ui.print(str, x, y, font, size)
  -- [TODO] How to change the font size of an already preloaded font face?
  -- The path is probably not cross-platform compatible.
  love.graphics.setNewFont(ui.fonts_dir .. "/" .. font .. ui.fonts_ext,
    math.floor(size * ui.scale * 1.3))
  love.graphics.print(str, math.floor(x), math.floor(y))
end


-- Tile an image over a rectangular area
function ui.tile(img, x, y, width, height)
  local pos = { x, y }
  for hi = 1, math.ceil(height / ui.assets.images[img]:getHeight()) do
    pos[1] = 0
    for wi = 1, math.ceil(width / ui.assets.images[img]:getWidth()) do
      ui.draw(img, pos[1], pos[2])
      pos[1] = pos[1] + ui.get_size(img)[1]
    end
    pos[2] = pos[2] + ui.get_size(img)[2]
  end
end


------------------------------------------------------------------------------
-- Blocks
------------------------------------------------------------------------------

-- Title screen background
function ui.draw_title_view()
  ui.tile("main-menu-bg-tile", 0, 0, ui.max_width, ui.max_height)
  ui.draw("main-menu-title", ui.unit * 0.5,
    ui.center_pos("main-menu-title")[2])
end


-- Title screen main menu
function ui.draw_title_menu()
  ui.draw("main-menu-new", ui.unit * 10.4, ui.center_pos("main-menu-new")[2])
  ui.print("new", ui.unit * 10.3, ui.unit * 5, "calcutta-regular", 20)
  ui.draw("main-menu-load", ui.unit * 11.3, ui.center_pos("main-menu-load")[2])
  ui.print("load", ui.unit * 11.4, ui.unit * 5, "calcutta-regular", 20)
  ui.draw("main-menu-help", ui.unit * 12.4, ui.center_pos("main-menu-help")[2])
  ui.print("help", ui.unit * 12.5, ui.unit * 5, "calcutta-regular", 20)
  ui.draw("main-menu-settings", ui.unit * 13.6,
  ui.center_pos("main-menu-settings")[2])
  ui.print("settings", ui.unit * 13.5, ui.unit * 5, "calcutta-regular", 20)
  ui.draw("main-menu-quit", ui.unit * 14.8, ui.center_pos("main-menu-quit")[2])
  ui.print("quit", ui.unit * 14.9, ui.unit * 5, "calcutta-regular", 20)
end


-- Title screen new player selection menu
function ui.draw_new_player_menu()
  ui.print("play as", ui.unit * 11.6, ui.unit * 3.7, "calcutta-regular", 24)
  ui.draw("main-menu-player-yellow", ui.unit * 10.8,
    ui.center_pos("main-menu-player-yellow")[2])
  ui.print("summer", ui.unit * 10.7, ui.unit * 5, "calcutta-regular", 20)
  ui.draw("main-menu-player-blue", ui.unit * 12.5,
    ui.center_pos("main-menu-player-blue")[2])
  ui.print("winter", ui.unit * 12.5, ui.unit * 5, "calcutta-regular", 20)
  ui.draw("main-menu-back", ui.unit * 14.8, ui.center_pos("main-menu-back")[2])
  ui.print("back", ui.unit * 14.7, ui.unit * 5, "calcutta-regular", 20)
end


------------------------------------------------------------------------------
-- Callback
------------------------------------------------------------------------------

function love.load()
  ui.init_assets(ui.images_dir, "images")
  ui.init_assets(ui.fonts_dir, "fonts")
end


function love.draw()
  ui.draw_title_view()
  ui.draw_title_menu()
  -- ui.draw_new_player_menu()
end
