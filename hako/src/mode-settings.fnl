(love.graphics.setNewFont 15)

(fn new-button [text __fn__]
  {: text :last false :fn __fn__ :now false})

(local buttons [])
(var state nil
(var font nil)

{:load (fn load []
           (set font (love.graphics.newFont 32))
           (table.insert buttons
                         (new-button :Music
                                     (fn []
                                       (print "Audio enable/disable"))))
           (table.insert buttons
                         (new-button :Return
                                     (fn []
                                       (print "Exiting..")
                                       (set state :mode-menu)))))
 :draw (fn draw [message]
         (let [ww (love.graphics.getWidth)
               wh (love.graphics.getHeight)
               button-height 64
               button-width (* ww (/ 1 3))
               margin 16]
           (var cursor-y 0)
           (local total-height (* (+ button-height margin) (length buttons)))
           (each [i button (ipairs buttons)]
             (set button.last button.now)
             (local bx (- (* ww 0.5) (* button-width 0.5)))
             (local by (+ (- (* wh 0.5) (* total-height 0.5)) cursor-y))
             (var color {1 0.4 2 0.4 3 0.5 4 1.0})
             (local (mx my) (love.mouse.getPosition))
             (local hot (and (and (and (> mx bx) (< mx (+ bx button-width))) (> my by))
                             (< my (+ by button-height))))
             (when hot
               (set color {1 0.8 2 0.8 3 0.9 4 1.0}))
             (set button.now (love.mouse.isDown 1))
             (when (and (and button.now (not button.last)) hot)
               (button.fn))
             (love.graphics.setColor (unpack color))
             (local text-w (font:getWidth button.text))
             (local text-h (font:getHeight button.text))
             (love.graphics.print button.text font (- (* ww 0.5) (* text-w 0.5))
                                  (+ by (* text-h 0.5)))
             (set cursor-y (+ cursor-y (+ button-height margin))))))
 :update (fn update [dt set-mode])
 :keypressed (fn keypressed [key set-mode]
               (when (= key :escape)
                 (love.event.quit)))}
