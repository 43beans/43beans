with import <nixpkgs> { };

let
  fennel_jit = import ./default.nix;
  pkgs = import <nixpkgs> { overlays = [
    (self: super:
      with super; {
        fennel = super.fennel.override { lua = super.luajit; };
      })
  ]; };
in pkgs.mkShell {
  packages = [
    pkgs.gnumake
    pkgs.zip
    pkgs.love_11
    pkgs.fennel
    pkgs.luajit
    pkgs.luarocks
    luajitPackages.luasocket
    luajitPackages.luasec
    luajitPackages.cjson
    luajitPackages.penlight
    luajitPackages.busted
  ];

  #We can include shared variables like this:
  #Name_Of_Envvar = "Something";
}
