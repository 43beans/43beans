# hako

- status: **under development**
- scope: game jam-level (so we can see what it's like to gamedev as a team)

## team

- dozens can help with backstory/symbolism
- mio can help with 2d graphics
- nico will do music/sfx and can help with code/design maybe
- marcus is a coder/code reviewer; will research and prototype ideas
- wsinatra is happy to code/review as well
- agafnd can contribute many kinds of sounds, some graphics (simple vector or
  raster (pixel?) art)
- acdw can do story and a little programming

## programming languages/frameworks

- fennel (potentially with love2d)
- previous suggestions: lua/love2d, godot

## gamejam?

- yes! [Fennel Game Jam 1](https://itch.io/jam/fennel-game-jam-1) (suggested by
  wsinatra)
- timeframe: **2022-05-25 07:00:00 - 2022-06-01 07:00:00**
