note: these are design notes from my own implimentaion of hako in typescript and my not be relevent or even useful in this context. ~dozens

## Design

this program is designed using *README driven development* and written in the *bricoleur* style:

- <https://ponyfoo.com/articles/readme-driven-development>
- <https://en.wikipedia.org/wiki/Bricolage#Internet>

the core classes of this program are:

- `face`: which represents the die, its edges and orientation and position; and
- `game`: which knows about state, stages of play, legal and illegal moves, and win conditions.

Goals:

- hako game notation: the ability to export/import a game. Probably just a sequence of insertions and deletions (stage 2) of position `{ x: number, y: number }` and orientation `Array<number>` (with 4 die edges represented sequentially "north, east, south, west")
    example:

    ```
    + 1111 @ 0,0
    + 0110 @ 0,-1
    + 1110 @ 1,-1
    ...
    - ???? @ 1,-1 // deletes: edges are ignored, only position is important
    + 0000 @ 2, 0 // a delete must be followed by an insert
    ```
    whitespace is ignored?

- UI: mvp = terminal; complete = web; reach = multiplayer server with websockets and invite code, etc.

- multiplayer: mvp = player v player local (hand off); complete = player v computer; reach = player v player networked

## color

colors for eventual webui. muted blue and yellow from ditherit.com should do nicely for night v. day

- https://www.color-hex.com/color/FFF585
- https://www.color-hex.com/color/134E87

