hako is a strategy dice game

<https://wiki.xxiivv.com/site/hako.html>

## setup: hako dice

Each player has four six-sided hako dice.

Each die face is divided into quarter triangles, which are either blank, or painted.

These are the die faces (0 and 1 represent edges of the die; 0 is blank/white and 1 is painted/red/black):

```
 0
0 0
 0
```

- Name: Day
- Description: All white, no painted edges.

```
 1
1 1
 1
```

- Name: Night
- Description: All colored.

```
 0
1 1
 1
```

- Name: Dawn/Morning
- Description: Mostly night, partial day. Three colored (night) edges, with one side of day (white)

```
 0
0 0
 1
```

- Name: Dusk/Evening
- Description: Mostly day, partial night. Three day, one night.

```
 1
0 1
 0
```

- Name: Spades/Sowing
- Description: Even amount of day and night, with a day/day/night/night pattern around the edges.

```
 1
0 0
 1
```

- Name: Cups/Harvest
- Description: even amount of day and night in a repeating day/night pattern around the edges. (it looks like an hourglass, a bowtie, or a cup)

## objective

> It's a little bit like Go

One player plays as DAY and one as NIGHT.

DAY wins if they surround any one dice on the playing field with 4 day edges.

NIGHT wins if they do the same with 4 night edges.

The example below is a win for NIGHT:

```
      0
     1 0
      1

 0    0    0
1 1  0 0  1 0
 0    0    0

      1
     1 1
      0
```

The center die is touched on all sides by the night edge of each surrounding dice.

## stage 1: summer

The first player rolls a die onto the playing field.

This is the starting die, the cornerstone.

Players then take turns rolling dice and placing them on the field.

You can orient your die in any direction, and attach them to any other die, edge to edge. (No diagonal placement.)

## stage 2: winter

After you have placed your four dice, turns continue as before, except that now on your turn you can take any die from the playing field, re-roll it, and re-place it anywhere you want, with the following caveats:

1. you cannot take the die that was played last.

2. you cannot take a die if doing so will leave any other die without a non-cardinal neighbor. That is, if it is 'floating' without touching another die edge-to-edge.
