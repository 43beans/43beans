abstract elements of the game open to interpretation.

## passage of time

The faces of each hako dice symbolize the passage of a full day.

In order:

1. Dawn
2. Day
5. Dusk
6. Night

Note: probably ignore cups and spades in the day/night cycle.

It is possible over the course of play for several "days" to pass.

For each roll, as long as the value continues to increase, it is counted as part of the same day.

E.g. 'Dawn, day, dusk' would all be considered a single day.

But as soon as you roll a vale less than the previous one, that must be considered the next day.

E.g. given rolls of 'Dawn, day, dusk, day', 'dawn, day, dusk' can be considered day 1, and day = the next day, day 2.

And so it is possible, and may be of symbolic value, to keep track of how much 'time' has passed during a game.

## possible endings

| winner | summer | winter  |
|--------|--------|---------|
| night  |   ??   |    ??   |
| day    |   ??   |    ??   |
| draw   |   ??   |    ??   |

What does it mean if the game ends during the phase 1, the summer phase? Seems like a bad sign for the loser to me: they don't even get to harvest their crops at all.

## that which is swallowed

What about the face that is surrounded at the end of the game?

What does it mean if night swallows day? Or day swallows dusk?

## cups and spades

cups and spades seem to exist outside the established day/night cycle. But may fit into the agricultural motif in that spades may dig the earth and sow the seeds. And cups represent bounty, baskets of harvest, the cornucopia.

I'm not sure about the scope of that though: I still think that phase 1 vs phase 2 ought to represent sowing vs reaping. so what significance does that leave for cups and spades? they could continue to represent investment and reward / giving and taking on some smaller scale. Or they could represent something else altogether based on more traditional symbolism in which spades represent the masculine and aggressive, and cups the feminine and receptive.
