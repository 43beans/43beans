# UI mockups

## game-logo

- "mako" name jokingly suggested by dozens, a placeholder name until game
  title is officially decided.

- It introduces the main colours, i.e. yellow/blue from dozens' dice faces.
  The letters are made from dice faces.

## intro-view, intro-view-select-menu-item

- The title screen with main menu options on the right.

- When a menu item is pressed or has a cursor hovered over it, it turns
  yellow.

- Stretch goal: add longer transition effect on mouse over or press down.
  - "new": colour fills yellow from left to right.
  - "load": colour fills yellow from bottom to top.
  - "help": colour fills yellow from left to right starting from the bottom
    left point.
  - "settings": yellow colour expands out from the center radially.
  - "quit": yellow colour expands in from the edges radially.

## intro-view-select-player

- After selecting "new" to start a new game, present option to play in yellow
  or blue.

- There is also a "back" button to return to the main menu.

## game-view

- Top left corner: a die tray where dice rolls are made. A border around the
  tray indicates player's turn, i.e. yellow border for day and blue for night.
  Two rows of small yellow and blue squares indicate the number of dice
  remaining per party in round 1 (each party starts with 4 dice).

- The background is a checkerboard pattern that acts as a guide for placement
  on a grid.

- When a die roll is completed, green squares on the board shows the available
  valid positions for the current move.

- Right column: a window where a story unfolds as player makes moves. To the
  right of the text near the top are small scroll up and down buttons to
  scroll the text output.

- Top right corner: an up arrow to hide the story window. When the story
  window is hidden, the arrow changes to a down arrow that can be pressed to
  reveal the window again. The button in the corner toggles the main menu.

## game-view-dice-sets

- Stretch goal: option to choose between the default and an additional flower
  dice set.

## game-view-select-roll-die

- Tap on a die to select it. Tap the roll button in the die tray to place
  the die on die tray and roll the selected die.

## game-view-confirm-move-die, game-view-confirm-rotate-die

- After rolling, tap on a valid empty space on the grid to place the die into
  its new position. A stippled border appears around the die, indicating the
  die is selected, with a sub-menu containing two buttons to cancel (red) or
  confirm (green) the move. Pressing cancel resets any changes in the current
  turn, and pressing confirm applies the move and ends the player turn.

- Tap once to select a die and enter move mode. Tap again to switch to rotate
  mode. Tap one more time on the die or in the background to deselect the die.

- In move mode, hold and drag the die (or within the selection bounds) to move
  it into another position, if available. In rotate mode, hold and drag the
  die (or the area around the die within the rotate symbol) to rotate the die
  in 90° intervals.

- The same sub-menu applies to rotate mode, therefore the operation order is
  to move the die into one of the available positions, rotate if needed then
  confirm the move.

## game-view-colour-shift-*

- The board shifts colour if one party is closer to winning. The criteria for
  this is to be confirmed.

## game-view-outcome-*

- The game outcome is announced in a message window on an overlay tinted with
  the winning party's colour. The message includes a button to start a new
  game.

- The overlay disables most of the interactive elements on the game board
  except the main menu button in the top right corner, which must always be
  available during game view to enable the player to quit the game at any
  time.

- Tapping anywhere outside of the message window dismisses the message and
  returns the view back to the board.

## game-view-main-menu

- A smaller version of the main menu in the intro view overlaid on top of the
  active game.

## help-view

- "help"/"how to play": to include gameplay instructions. Text is arranged in
  two columns. The left and right arrows at the bottom of the screen scroll
  forward and backward between multiple pages. The left arrow is dimmed (40%
  opacity) on the first page and the right arrow is dimmed on the last page to
  indicate the page cannot be scrolled further in the corresponding
  directions.

- "about": to include version number and individual credits in a single
  centered text column.

- The "back" button closes the help view and returns the player to the intro
  view or the active game main menu, depending on the location from which the
  player accessed the help button.
